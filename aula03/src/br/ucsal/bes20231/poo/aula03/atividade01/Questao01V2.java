package br.ucsal.bes20231.poo.aula03.atividade01;

import java.util.Scanner;

public class Questao01V2 {

	public static void main(String[] args) {
		obterNotaExibirConceito();
	}

	private static void obterNotaExibirConceito() {
		int nota;
		String conceito;

		nota = obterNota();
		conceito = calcularConceito(nota);
		exibirConceito(conceito);
	}

	private static void exibirConceito(String conceito) {
		System.out.println("Conceito=" + conceito);
	}

	private static String calcularConceito(int nota) {
		String conceito;
		if (nota <= 49) {
			conceito = "Insuficiente";
		} else if (nota <= 64) {
			conceito = "Regular";
		} else if (nota <= 84) {
			conceito = "Bom";
		} else {
			conceito = "Ótimo";
		}
		return conceito;
	}

	private static int obterNota() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe a nota (0 a 100), intervalo fechado):");
		return scanner.nextInt();
	}
}
