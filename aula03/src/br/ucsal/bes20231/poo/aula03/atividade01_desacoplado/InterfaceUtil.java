package br.ucsal.bes20231.poo.aula03.atividade01_desacoplado;

public class InterfaceUtil {

	private static InterfaceUtil instance;

	private Interface interface_ = new GuiUtil();

	public static InterfaceUtil get() {
		if (instance == null) {
			instance = new InterfaceUtil();
		}
		return instance;
	}

	public int obterInteiro(String mensagem) {
		return interface_.obterInteiro(mensagem);
	}

	public void exibirMensagem(String mensagem) {
		interface_.exibirMensagem(mensagem);
	}
}
