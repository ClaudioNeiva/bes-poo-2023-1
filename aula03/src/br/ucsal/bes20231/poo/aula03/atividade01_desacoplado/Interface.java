package br.ucsal.bes20231.poo.aula03.atividade01_desacoplado;

public interface Interface {

	int obterInteiro(String mensagem);

	void exibirMensagem(String mensagem);

}
