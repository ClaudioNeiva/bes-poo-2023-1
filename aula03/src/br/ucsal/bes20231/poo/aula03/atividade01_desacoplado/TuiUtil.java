package br.ucsal.bes20231.poo.aula03.atividade01_desacoplado;

import java.util.Scanner;

public class TuiUtil implements Interface {

	public int obterInteiro(String mensagem) {
		Scanner scanner = new Scanner(System.in);
		exibirMensagem(mensagem);
		return scanner.nextInt();
	}

	public void exibirMensagem(String mensagem) {
		System.out.println(mensagem);
	}

}
