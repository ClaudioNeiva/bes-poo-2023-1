package br.ucsal.bes20231.poo.aula04.curiosidade;

import java.math.BigDecimal;

public class Curiosidade {

	/*
	 	a=1239.843
		c=28516.389000000003
		a1=1239.843
		c1=28516.389
	 */
	 
	public static void main(String[] args) {
		double a = 1239.843;
		int b = 23;
		double c = a * b;
		System.out.println("a=" + a);
		System.out.println("c=" + c);

		BigDecimal a1 = new BigDecimal("1239.843");
		BigDecimal c1 = a1.multiply(BigDecimal.valueOf(b));
		System.out.println("a1=" + a1);
		System.out.println("c1=" + c1);
	}

}
