package br.ucsal.bes20231.poo.aula04.atividade01;

import java.util.Scanner;

public class Questao3 {

	private static final int QTD_NUMEROS = 10;

	public static void main(String[] args) {
		calcularMaiorMenorMedia();
	}

	private static void calcularMaiorMenorMedia() {
		int n;
		int soma = 0;
		// int maior = Integer.MIN_VALUE;
		int maior = 0;
		int menor = 0;
		float media;
		Scanner scanner = new Scanner(System.in);

		for (int i = 0; i < QTD_NUMEROS; i++) {
			// Entrada
			System.out.print("Informe o numero " + (i + 1) + ":");
			n = scanner.nextInt();

			// Processamento
			soma += n;
			if (i == 0) {
				maior = n;
				menor = n;
			} else {
				if (n > maior) {
					maior = n;
				}
				if (n < menor) {
					menor = n;
				}
			}
		}
		media = soma / (float) QTD_NUMEROS;

		// Saída
		System.out.println("Media=" + media);
		System.out.println("Maior=" + maior);
		System.out.println("Menor=" + menor);
	}

}
