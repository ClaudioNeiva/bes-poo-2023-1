package br.ucsal.bes20231.poo.aula08;

public class Exemplo {

	public static void main(String[] args) {

		System.out.println("Aluno.contador=" + Aluno.getContador());

		Aluno aluno1 = new Aluno("claudio");

		Aluno aluno2 = new Aluno("maria");

		System.out.println("Aluno.contador=" + Aluno.getContador());
		
		System.out.println("aluno1.matricula=" + aluno1.getMatricula());
		System.out.println("aluno1.nome=" + aluno1.getNome());

		System.out.println("aluno2.matricula=" + aluno2.getMatricula());
		System.out.println("aluno2.nome=" + aluno2.getNome());

	}

}
