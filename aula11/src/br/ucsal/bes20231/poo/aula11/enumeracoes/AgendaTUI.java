package br.ucsal.bes20231.poo.aula11.enumeracoes;

public class AgendaTUI {

	private static int TIPO_REUNIAO_PRESENCIAL = 1;

	private static int TIPO_REUNIAO_ONLINE = 2;
	
	public static void cadastrar() {
		System.out.println("Executando as atividades de cadastro...");
	}

	public static void consultar() {
		System.out.println("Executando as atividades de consulta...");
	}

	public static void alterar() {
		System.out.println("Executando as atividades de alteração...");
	}

	public static void excluir() {
		System.out.println("Executando as atividades de exclusão...");
	}

}
