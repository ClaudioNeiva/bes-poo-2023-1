package br.ucsal.bes20231.poo.aula11.enumeracoes;

public class Exemplo {

	public static void main(String[] args) {
		System.out.println(calcularE(1));
		System.out.println(calcularE(2));
		System.out.println(calcularE(3));
		System.out.println(calcularE(4));
	}

	private static double calcularE(int n) {
		double e = 0;
		int i = 0;
		while (i <= n) {
			e += 1. / calcularFatorial(i);
			i++;
		}
		return e;
	}

	private static double calcularFatorial(int n) {
		long fatorial = 1;
		for (int i = 1; i <= n; i++) {
			fatorial *= i;
		}
		return fatorial;
	}

}
