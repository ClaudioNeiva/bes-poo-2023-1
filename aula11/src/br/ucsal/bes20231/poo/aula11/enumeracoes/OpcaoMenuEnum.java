package br.ucsal.bes20231.poo.aula11.enumeracoes;

public enum OpcaoMenuEnum {

	EXCLUIR(1, "Excluir"),

	ALTERAR(2, "Alterar"),

	CONSULTAR(3, "Consultar"),

	CADASTRAR(4, "Cadastrar"),

	SAIR(9, "Sair do sistema");

	private int codigo;

	private String descricao;

	private OpcaoMenuEnum(int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getDescricaoCompleta() {
		return codigo + " - " + descricao;
	}

	public static OpcaoMenuEnum valueOfCodigo(int codigo) {
		for (OpcaoMenuEnum opcaoMenu : values()) {
			if (opcaoMenu.codigo == codigo) {
				return opcaoMenu;
			}
		}
		throw new IllegalArgumentException(
				OpcaoMenuEnum.class.getClass().getName() + "." + codigo + " não encontrado.");
	}

}
