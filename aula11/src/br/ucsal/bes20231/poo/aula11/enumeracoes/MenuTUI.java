package br.ucsal.bes20231.poo.aula11.enumeracoes;

import java.util.Scanner;

public class MenuTUI {

	private static Scanner scanner = new Scanner(System.in);

	private static long i = 0;

	public static void main(String[] args) {
		executar();
	}

	private static void executar() {
		OpcaoMenuEnum opcao;
		do {
			exibirOpcoes();
			opcao = selecionarOpcao();
			executarOpcao(opcao);
		} while (!opcao.equals(OpcaoMenuEnum.SAIR));
	}

	private static void exibirOpcoes() {
		for (OpcaoMenuEnum opcao : OpcaoMenuEnum.values()) {
			System.out.println(opcao.getDescricaoCompleta());
		}
	}

	private static OpcaoMenuEnum selecionarOpcao() {
		OpcaoMenuEnum opcaoSelecionada;
		int opcaoSelecionadaInt;
		System.out.println("Informe o código da opção desejada:");
		opcaoSelecionadaInt = scanner.nextInt();
		scanner.nextLine(); // Limpeza de buffer após leitura de inteiro.

		// Converter opcaoSelecionadaString em opcaoSelecionada;
		opcaoSelecionada = OpcaoMenuEnum.valueOfCodigo(opcaoSelecionadaInt);
		
		return opcaoSelecionada;
	}

	private static void executarOpcao(OpcaoMenuEnum opcao) {
		switch (opcao) {
		case CADASTRAR:
			AgendaTUI.cadastrar();
			break;
		case CONSULTAR:
			AgendaTUI.consultar();
			break;
		case ALTERAR:
			AgendaTUI.alterar();
			break;
		case EXCLUIR:
			AgendaTUI.excluir();
			break;
		case SAIR:
			System.out.println("Bye...");
			break;
		}
	}

}
