package br.ucsal.bes20231.poo.aula11.heranca.semheranca1;

import java.util.List;

public class PessoaJuridica {

	String cnpj;

	String nome;
	
	String email;

	List<String> telefones;

	int inscricaoEstadual;

	int inscricaoMunicipal;
	
	List<Endereco> enderecos;

}
