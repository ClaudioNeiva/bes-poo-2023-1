package br.ucsal.bes20231.poo.aula11.heranca.comheranca;

import java.util.List;

import br.ucsal.bes20231.poo.aula11.heranca.semheranca2.Endereco;

public abstract class Pessoa {

	String nome;

	List<String> telefones;

	List<Endereco> enderecos;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}
	
	
}
