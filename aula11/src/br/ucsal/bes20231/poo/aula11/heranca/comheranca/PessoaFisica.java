package br.ucsal.bes20231.poo.aula11.heranca.comheranca;

public class PessoaFisica extends Pessoa {

	String cpf;

	String nomeMae;

	int pis;

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public int getPis() {
		return pis;
	}

	public void setPis(int pis) {
		this.pis = pis;
	}

}
