package br.ucsal.bes20231.poo.aula11.heranca.semheranca2;

import java.util.List;

public class Pessoa {

	String cpf;

	String nomeMae;

	int pis;

	String cnpj;

	String nome;

	List<String> telefones;

	int inscricaoEstadual;

	int inscricaoMunicipal;
	
	List<Endereco> enderecos;
}
