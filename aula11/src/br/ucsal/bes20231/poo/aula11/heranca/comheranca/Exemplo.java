package br.ucsal.bes20231.poo.aula11.heranca.comheranca;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Exemplo {

	public static void main(String[] args) {

		List<Pessoa> clientes = new ArrayList<>();

		// Pessoa pessoa = new Pessoa();

		PessoaFisica pessoaFisica = new PessoaFisica();
		PessoaJuridica pessoaJuridica = new PessoaJuridica();

		pessoaFisica.cpf = "123";
		pessoaFisica.nome = "claudio";

		pessoaJuridica.cnpj = "456";
		pessoaJuridica.nome = "empresa x";

		clientes.add(pessoaFisica);
		clientes.add(pessoaJuridica);

		apresentar(pessoaFisica);
		apresentar(pessoaJuridica);

	}

	// ATENÇÃO! Na verdade essa "necessidade" está mais ligada à uma modelagem ruim
	// do que a uma real necessidade do recurso que aqui será exibido.
	private static void apresentar(Pessoa pessoa) {
		System.out.println("nome=" + pessoa.nome);
		// Exibir o cpf para pessoa física e cnpj para pessoa jurídica
		if (pessoa instanceof PessoaFisica pessoaFisica) {
			System.out.println("cpf=" + pessoaFisica.cpf);
		} else {
			PessoaJuridica pessoaJuridica = (PessoaJuridica) pessoa;
			System.out.println("cnpj=" + pessoaJuridica.cnpj);
		}
	}

	private static void apresentar(PessoaFisica pessoaFisica) {
		System.out.println("nome=" + pessoaFisica.nome);
		System.out.println("cpf=" + pessoaFisica.cpf);
	}
	
	private static void apresentar(PessoaJuridica pessoaJuridica) {
		System.out.println("nome=" + pessoaJuridica.nome);
		System.out.println("cnpj=" + pessoaJuridica.cnpj);
	}
}




