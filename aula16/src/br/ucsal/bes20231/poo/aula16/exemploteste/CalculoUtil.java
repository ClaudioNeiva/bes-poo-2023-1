package br.ucsal.bes20231.poo.aula16.exemploteste;

public class CalculoUtil {

	public static long calcularFatorial(int n) {
		if (n == 0) {
			return 1;
		}
		return n * calcularFatorial(n - 1);
	}

}
