package br.ucsal.bes20231.poo.aula16.ordenacao;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ExemploOrdenacaoString {

	public static void main(String[] args) {

		// List<String> nomes = new ArrayList<>();
		// nomes.add("jailton");
		// nomes.add("claudio");
		// nomes.add("daniel");
		// nomes.add("antonio");
		// nomes.add("maria");
		// nomes.add("ana");

		List<String> nomes = Arrays.asList("jailton", "claudio", "Daniel", "antonio", "maria", "ana");

		System.out.println("Nomes na ordem que foram incluídos na lista:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

		System.out.println(
				"\nNomes em ordem crescente, mas com problemas para trabalhar com letras maiúsculas e minúsculas misturadas:");
		nomes.sort(Comparator.naturalOrder());
		for (String nome : nomes) {
			System.out.println(nome);
		}

		System.out.println("\nNomes em ordem crescente:");
		nomes.sort(String.CASE_INSENSITIVE_ORDER);
		for (String nome : nomes) {
			System.out.println(nome);
		}

		System.out.println(
				"\nNomes em ordem decrescente, mas com problemas para trabalhar com letras maiúsculas e minúsculas misturadas:");
		nomes.sort(Comparator.reverseOrder());
		for (String nome : nomes) {
			System.out.println(nome);
		}

		System.out.println(
				"\nNomes em ordem decrescente:");
		nomes.sort(String.CASE_INSENSITIVE_ORDER);
		Collections.reverse(nomes);
		for (String nome : nomes) {
			System.out.println(nome);
		}

	}

}
