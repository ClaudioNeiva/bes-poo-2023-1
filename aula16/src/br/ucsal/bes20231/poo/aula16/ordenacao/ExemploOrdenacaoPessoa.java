package br.ucsal.bes20231.poo.aula16.ordenacao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ExemploOrdenacaoPessoa {

	public static void main(String[] args) {

		List<Pessoa> pessoas = new ArrayList<>();

		pessoas.add(new Pessoa(5, "claudio", "neiva", LocalDate.of(1975, 5, 9)));
		pessoas.add(new Pessoa(9, "daniel", "melo", LocalDate.of(2005, 8, 12)));
		pessoas.add(new Pessoa(7, "mateus", "neiva", LocalDate.of(2004, 9, 10)));
		pessoas.add(new Pessoa(2, "joao", "melo", LocalDate.of(1980, 12, 11)));
		pessoas.add(new Pessoa(8, "jailton", "cruz", LocalDate.of(2006, 7, 2)));

		System.out.println("Pessoas na ordem que foram incluídas na lista:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		System.out.println("\nPessoas em ordem crescente de data de nascimento:");
		pessoas.sort(Comparator.naturalOrder());
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		System.out.println("\nPessoas em ordem decrescente de data de nascimento:");
		pessoas.sort(Comparator.reverseOrder());
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		System.out.println("\nPessoas em ordem crescente de nome:");
		pessoas.sort(Comparator.comparing(Pessoa::getNome));
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		System.out.println(
				"\nPessoas em ordem crescente de sobrenome, e para sobrenomes iguais, ordem crescente de nome:");
		pessoas.sort(Comparator.comparing(Pessoa::getSobrenome).thenComparing(Pessoa::getNome));
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		// Como era feito até o Java 1.7:
		System.out.println("\nPessoas em ordem crescente de nome:");
		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa o1, Pessoa o2) {
				return o1.getNome().compareTo(o2.getNome());
			}

		});
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

	}

}
