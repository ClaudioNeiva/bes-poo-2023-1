package br.ucsal.bes20231.poo.aula16.ordenacao;

import java.time.LocalDate;

public class Pessoa implements Comparable<Pessoa> {

	private Integer codigo;

	private String nome;

	private String sobrenome;

	private LocalDate dataNascimento;

	public Pessoa(Integer codigo, String nome, String sobrenome, LocalDate dataNascimento) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.dataNascimento = dataNascimento;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	@Override
	public String toString() {
		return "Pessoa [codigo=" + codigo + ", nome=" + nome + ", sobrenome=" + sobrenome + ", dataNascimento="
				+ dataNascimento + "]";
	}

	@Override
	public int compareTo(Pessoa o) {
		return dataNascimento.compareTo(o.dataNascimento);
	}

}
