package br.ucsal.bes20231.poo.aula16.ordenacao;

import java.util.Arrays;
import java.util.List;

public class ExemploOrdenacaoStringFormasDiferentesListagem {

	public static void main(String[] args) {

		// List<String> nomes = new ArrayList<>();
		// nomes.add("jailton");
		// nomes.add("claudio");
		// nomes.add("daniel");
		// nomes.add("antonio");
		// nomes.add("maria");
		// nomes.add("ana");

		List<String> nomes = Arrays.asList("jailton", "claudio", "daniel", "antonio", "maria", "ana");

		System.out.println("Nomes na ordem que foram incluídos na lista:");
		
		// forma1:
		// for (int i = 0; i < nomes.size(); i++) {
		// String nome = nomes.get(i);
		// System.out.println(nome);
		// }

		// forma2:
		// for (String nome : nomes) {
		// System.out.println(nome);
		// }

		// forma3:
		// nomes.forEach(nome -> System.out.println(nome));

		// forma4:
		nomes.forEach(System.out::println);

	}

}
