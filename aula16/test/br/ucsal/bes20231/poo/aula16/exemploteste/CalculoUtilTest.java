package br.ucsal.bes20231.poo.aula16.exemploteste;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculoUtilTest {

	@Test
	void testarFatorial4() {
		int n = 4;
		long fatorialEsperado = 24;
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	void testarFatorial6() {
		int n = 6;
		long fatorialEsperado = 720;
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
