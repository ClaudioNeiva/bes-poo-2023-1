package br.ucsal.bes20231.poo.aula17.conjuntos;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class ExemploConjuntoPessoa {

	public static void main(String[] args) {

		Set<Pessoa> pessoas = new HashSet<>();

		pessoas.add(new Pessoa(5, "claudio", "neiva", LocalDate.of(1975, 5, 9)));
		pessoas.add(new Pessoa(9, "daniel", "melo", LocalDate.of(2005, 8, 12)));
		pessoas.add(new Pessoa(7, "mateus", "neiva", LocalDate.of(2004, 9, 10)));
		pessoas.add(new Pessoa(2, "joao", "melo", LocalDate.of(1980, 12, 11)));
		pessoas.add(new Pessoa(8, "jailton", "cruz", LocalDate.of(2006, 7, 2)));
		pessoas.add(new Pessoa(7, "mateus", "neiva", LocalDate.of(2004, 9, 10)));
		pessoas.add(new Pessoa(5, "claudio", "neiva", LocalDate.of(1975, 5, 9)));
		pessoas.add(new Pessoa(5, "claudio", "neiva", LocalDate.of(1975, 5, 9)));

		System.out.println("Pessoas que estão no conjunto:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}
		System.out.println("\n\n");

		Pessoa novaPessoa = new Pessoa(18, "manuela", "neiva", LocalDate.of(2004, 4, 12));
		System.out.println("novaPessoa=" + novaPessoa);
		System.out.println("novaPessoa.hashCode=" + novaPessoa.hashCode());
		pessoas.add(novaPessoa);
		System.out.println("pessoas.contains(novaPessoa)=" + pessoas.contains(novaPessoa));
		novaPessoa.setCodigo(25);
		System.out.println("novaPessoa.hashCode=" + novaPessoa.hashCode());
		System.out.println("novaPessoa=" + novaPessoa);
		System.out.println("pessoas.contains(novaPessoa)=" + pessoas.contains(novaPessoa));
		System.out.println("Pessoas que estão no conjunto:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

	}

}
