package br.ucsal.bes20231.poo.aula17.conjuntos;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ExemploSemConjunto1 {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		List<Integer> numerosDistintos = obterNumerosDistintos(5);
		listarNumeros("\nNúmeros distintos informados:", numerosDistintos);
	}

	/**
	 * Crie uma programa que solicite do usuário n números distintos.
	 * 
	 * @param qtd quantidade de números a serem solicitados
	 */
	private static List<Integer> obterNumerosDistintos(int qtd) {
		List<Integer> numerosDistintos = new ArrayList<>();
		Integer n;
		System.out.println("Informe " + qtd + " números distintos (usando List):");
		do {
			n = scanner.nextInt();
			if (!numerosDistintos.contains(n)) {
				numerosDistintos.add(n);
			}
		} while (numerosDistintos.size() < qtd);
		return numerosDistintos;
	}

	private static void listarNumeros(String mensagem, List<Integer> numeros) {
		System.out.println(mensagem);
		numeros.forEach(System.out::println);
//		for(Integer numero: numeros) {
//			System.out.println(numero);
//		}
	}

}
