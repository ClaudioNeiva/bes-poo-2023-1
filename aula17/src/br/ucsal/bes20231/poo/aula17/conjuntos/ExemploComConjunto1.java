package br.ucsal.bes20231.poo.aula17.conjuntos;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class ExemploComConjunto1 {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		Set<Integer> numerosDistintos = obterNumerosDistintos(5);
		listarNumeros("\nNúmeros distintos informados:", numerosDistintos);
	}

	/**
	 * Crie uma programa que solicite do usuário n números distintos.
	 * 
	 * @param qtd quantidade de números a serem solicitados
	 */
	private static Set<Integer> obterNumerosDistintos(int qtd) {
		Set<Integer> numerosDistintos = new HashSet<>();
		// Set<Integer> numerosDistintos = new LinkedHashSet<>();
		// Set<Integer> numerosDistintos = new TreeSet<>();

		System.out.println("Informe " + qtd + " números distintos (usando Set):");
		do {
			numerosDistintos.add(scanner.nextInt());
		} while (numerosDistintos.size() < qtd);
		return numerosDistintos;
	}

	private static void listarNumeros(String mensagem, Set<Integer> numeros) {
		System.out.println(mensagem);
		numeros.forEach(System.out::println);
//		for(Integer numero: numeros) {
//			System.out.println(numero);
//		}
	}

}
