package br.ucsal.bes20231.poo.aula12.domain;

import br.ucsal.bes20231.poo.aula12.exception.NegocioException;

public class ContaCorrente {

	private Integer numero;

	private String nomeCorrentista;

	private Double saldo;

	private Boolean isBloqueada;

	public ContaCorrente(Integer numero, String nomeCorrentista) {
		super();
		this.numero = numero;
		this.nomeCorrentista = nomeCorrentista;
		this.saldo = 0.;
		this.isBloqueada = false;
	}

	public void depositar(Double valor) {
		saldo += valor;
	}

	public void sacar(Double valor) throws NegocioException  {
		if (isBloqueada) {
			throw new NegocioException("Conta bloqueada.");
		}
		if (valor > saldo) {
			throw new NegocioException("Saldo insuficiente.");
		}
		saldo -= valor;
	}

	public Double consultarSaldo() {
		return saldo;
	}

	public Integer getNumero() {
		return numero;
	}

	public String getNomeCorrentista() {
		return nomeCorrentista;
	}

}
