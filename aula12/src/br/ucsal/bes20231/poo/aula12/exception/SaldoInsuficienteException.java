package br.ucsal.bes20231.poo.aula12.exception;

public class SaldoInsuficienteException extends Exception {

	private static final long serialVersionUID = 1L;

	public SaldoInsuficienteException() {
		super("Saldo insuficiente;");
	}
	
}
