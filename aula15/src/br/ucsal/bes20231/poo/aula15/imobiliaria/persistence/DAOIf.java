package br.ucsal.bes20231.poo.aula15.imobiliaria.persistence;

import java.util.List;

public interface DAOIf<T> {

	void insert(T obj);

	void delete(T obj);

	List<T> findAll();

}
