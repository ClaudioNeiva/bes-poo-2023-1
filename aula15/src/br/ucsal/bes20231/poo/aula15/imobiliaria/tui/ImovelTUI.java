package br.ucsal.bes20231.poo.aula15.imobiliaria.tui;

import java.math.BigDecimal;
import java.util.List;

import br.ucsal.bes20231.poo.aula15.imobiliaria.domain.Apartamento;
import br.ucsal.bes20231.poo.aula15.imobiliaria.domain.Casa;
import br.ucsal.bes20231.poo.aula15.imobiliaria.domain.Imovel;
import br.ucsal.bes20231.poo.aula15.imobiliaria.exception.ValorImovelInvalidoException;
import br.ucsal.bes20231.poo.aula15.imobiliaria.persistence.ImovelDAO;

public class ImovelTUI {

	public static void main(String[] args) {
		executarMenu();
	}

	public static void executarMenu() {
		// TODO Este método precisa ser imlementado, apresentando as opções e permitindo
		// ao usuário selecionar a desejada;
		cadastrarCasa();
	}

	public static void cadastrarCasa() {
		Integer codigo = UtilTUI.obterInteger("Informe o código:");
		String endereco = UtilTUI.obterString("Informe o endereço:");
		String bairro = UtilTUI.obterString("Informe o bairro:");
		BigDecimal valor = UtilTUI.obterBigDecimal("Informe o valor:");
		BigDecimal areaTerreno = UtilTUI.obterBigDecimal("Informe a área de terreno:");
		BigDecimal areaConstruida = UtilTUI.obterBigDecimal("Informe a área construída:");

		try {
			Casa casa = new Casa(codigo, endereco, bairro, valor, areaTerreno, areaConstruida);
			ImovelDAO.insert(casa);
			System.out.println(casa);
		} catch (ValorImovelInvalidoException e) {
			System.out.println("Erro ao cadastrar casa: " + e.getMessage());
		}
	}

	public static void cadastrarApartamento() {

		Integer codigo = UtilTUI.obterInteger("Informe o código:");
		String endereco = UtilTUI.obterString("Informe o endereço:");
		String bairro = UtilTUI.obterString("Informe o bairro:");
		BigDecimal valor = UtilTUI.obterBigDecimal("Informe o valor:");
		BigDecimal areaFracaoIdeal = UtilTUI.obterBigDecimal("Informe a fração ideal:");
		BigDecimal areaPrivativa = UtilTUI.obterBigDecimal("Informe a área privativa:");

		try {
			Apartamento apartamento = new Apartamento(codigo, endereco, bairro, valor, areaFracaoIdeal, areaPrivativa);
			ImovelDAO.insert(apartamento);
		} catch (ValorImovelInvalidoException e) {
			System.out.println("Erro ao cadastrar casa: " + e.getMessage());
		}
	}

	public static void exibirImoveisPorCodigo() {
		List<Imovel> imoveis = ImovelDAO.findAllOrderByCodigo();
		System.out.println("Lista de imóveis ordenada por código:");
		for (Imovel imovel : imoveis) {
			System.out.println("************************");
			System.out.println("código:" + imovel.getCodigo());
			System.out.println("endereço:" + imovel.getEndereco());
			System.out.println("valor do imóvel:" + imovel.getBairro());
		}
	}

	public static void exibirImoveisPorBairroValor() {
		List<Imovel> imoveis = ImovelDAO.findAllOrderByBairroValor();
		System.out.println("Lista de imóveis ordenada por bairro e valor:");
		for (Imovel imovel : imoveis) {
			System.out.println("************************");
			System.out.println("código:" + imovel.getCodigo());
			System.out.println("endereço:" + imovel.getEndereco());
			System.out.println("valor do imóvel:" + imovel.getValor());
			System.out.println("valor do imposto:" + imovel.calcularValorImposto());
		}
	}

}
