package br.ucsal.bes20231.poo.aula15.imobiliaria.domain;

import java.math.BigDecimal;

import br.ucsal.bes20231.poo.aula15.imobiliaria.exception.ValorImovelInvalidoException;

/*
 * Os imóveis são caracterizados por: código (número inteiro), endereço (texto), bairro (texto) 
 * e valor (número fracionário). 
 * 
 * A imobiliária trabalha com apenas dois tipos de imóvel: apartamentos e casas. 
 */
public abstract class Imovel implements Comparable<Imovel>  {

	private Integer codigo;

	private String endereco;

	private String bairro;

	private BigDecimal valor;

	public Imovel(Integer codigo, String endereco, String bairro, BigDecimal valor)
			throws ValorImovelInvalidoException {
		super();
		this.codigo = codigo;
		this.endereco = endereco;
		this.bairro = bairro;
		setValor(valor);
	}

	public abstract BigDecimal calcularValorImposto();

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) throws ValorImovelInvalidoException {
		validarValor(valor);
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "Imovel [codigo=" + codigo + ", endereco=" + endereco + ", bairro=" + bairro + ", valor=" + valor + "]";
	}

	private void validarValor(BigDecimal valor) throws ValorImovelInvalidoException {
		// if(valor <= 0) { se valor fosse double, float, int, long, etc.
		if (valor.compareTo(BigDecimal.ZERO) <= 0) {
			throw new ValorImovelInvalidoException();
		}
	}

	@Override
	public int compareTo(Imovel o) {
		return codigo.compareTo(o.codigo);
	}

}
