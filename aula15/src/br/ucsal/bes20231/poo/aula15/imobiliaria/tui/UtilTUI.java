package br.ucsal.bes20231.poo.aula15.imobiliaria.tui;

import java.math.BigDecimal;
import java.util.Scanner;

public class UtilTUI {
	
	private static Scanner scanner = new Scanner(System.in);

	public static BigDecimal obterBigDecimal(String mensagem) {
		System.out.println(mensagem);
		return scanner.nextBigDecimal();
	}

	public static String obterString(String mensagem) {
		System.out.println(mensagem);
		return scanner.nextLine();
	}

	public static Integer obterInteger(String mensagem) {
		System.out.println(mensagem);
		Integer codigo = scanner.nextInt();
		scanner.nextLine();
		return codigo;
	}
}
