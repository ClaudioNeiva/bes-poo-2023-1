package br.ucsal.bes20231.poo.aula15.imobiliaria.exception;

public class ValorImovelInvalidoException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private static final String MENSAGEM = "O valor do imóvel deve ser maior que zero.";

	public ValorImovelInvalidoException() {
		super(MENSAGEM);
	}
}
