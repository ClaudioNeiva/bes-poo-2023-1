package br.ucsal.bes20231.poo.aula15.imobiliaria.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20231.poo.aula15.imobiliaria.domain.Imovel;

public class ImovelDAO2 implements DAOIf<Imovel> {

	private static ImovelDAO2 instance;

	private List<Imovel> imoveis = new ArrayList<>();

	public static ImovelDAO2 get() {
		if (instance == null) {
			instance = new ImovelDAO2();
		}
		return instance;
	}

	public void insert(Imovel imovel) {
		imoveis.add(imovel);
	}

	public void delete(Imovel imovel) {
		imoveis.remove(imovel);
	}

	public List<Imovel> findAll() {
		return new ArrayList<>(imoveis);
	}

}
