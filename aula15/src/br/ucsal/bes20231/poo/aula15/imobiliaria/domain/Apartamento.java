package br.ucsal.bes20231.poo.aula15.imobiliaria.domain;

import java.math.BigDecimal;

import br.ucsal.bes20231.poo.aula15.imobiliaria.exception.ValorImovelInvalidoException;

/*
 *  necessário saber também a 
 *  área de fração ideal (número fracionário) e a área privativa (número fracionário),
 */
public class Apartamento extends Imovel {

	private BigDecimal areaFracaoIdeal;

	private BigDecimal areaPrivativa;

	public Apartamento(Integer codigo, String endereco, String bairro, BigDecimal valor, BigDecimal areaFracaoIdeal,
			BigDecimal areaPrivativa) throws ValorImovelInvalidoException {
		super(codigo, endereco, bairro, valor);
		this.areaFracaoIdeal = areaFracaoIdeal;
		this.areaPrivativa = areaPrivativa;
	}

	@Override
	public BigDecimal calcularValorImposto() {
		// return areaPrivativa * 130 + areaFracaoIdeal * 40;
		return areaPrivativa.multiply(new BigDecimal(130)).add(areaFracaoIdeal.multiply(new BigDecimal(40)));
	}

	public BigDecimal getAreaFracaoIdeal() {
		return areaFracaoIdeal;
	}

	public void setAreaFracaoIdeal(BigDecimal areaFracaoIdeal) {
		this.areaFracaoIdeal = areaFracaoIdeal;
	}

	public BigDecimal getAreaPrivativa() {
		return areaPrivativa;
	}

	public void setAreaPrivativa(BigDecimal areaPrivativa) {
		this.areaPrivativa = areaPrivativa;
	}

	@Override
	public String toString() {
		return "Apartamento [areaFracaoIdeal=" + areaFracaoIdeal + ", areaPrivativa=" + areaPrivativa + ", toString()="
				+ super.toString() + "]";
	}

}
