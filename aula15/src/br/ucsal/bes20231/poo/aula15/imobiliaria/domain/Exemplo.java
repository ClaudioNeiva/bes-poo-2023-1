package br.ucsal.bes20231.poo.aula15.imobiliaria.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20231.poo.aula15.imobiliaria.exception.ValorImovelInvalidoException;

public class Exemplo {

	public static void main(String[] args) {

		// Imovel não pode ser instanciado, pois é uma classe abstrata.
		// Imovel imovel1 = new Imovel();

		List<Imovel> imoveis = new ArrayList<>();

		try {
			Apartamento apartamento1 = new Apartamento(10, "Rua w", "Bairro z", new BigDecimal("100000.00"),
					new BigDecimal("450.45"), new BigDecimal("220.12"));
			imoveis.add(apartamento1);

			Casa casa1 = new Casa(10, "Rua x", "Bairro y", new BigDecimal("2500000.00"), new BigDecimal("1000"),
					new BigDecimal("400.70"));
			imoveis.add(casa1);

		} catch (ValorImovelInvalidoException e) {
			System.out.println("Erro ao instanciar o imóvel: " + e.getMessage());
		}

		listarImoveis(imoveis);

	}

	private static void listarImoveis(List<Imovel> imoveis) {
		// código, endereço, valor do imóvel e valor do imposto
		for (Imovel imovel : imoveis) {
			System.out.println("\n************************");
			System.out.println("código=" + imovel.getCodigo());
			System.out.println("endereço=" + imovel.getEndereco());
			System.out.println("valor=" + imovel.getValor());
			System.out.println("imposto=" + imovel.calcularValorImposto());

			// Funciona, mas NÃO é a forma ideal de trabalho!
			//if (imovel instanceof Casa casa) {
			//	System.out.println("imposto=" + casa.calcularValorImposto());
			//} else if (imovel instanceof Apartamento apartamento) {
			//	System.out.println("imposto=" + apartamento.calcularValorImposto());
			//}

		}
	}

}
