package br.ucsal.bes20231.poo.aula15.imobiliaria.domain;

import java.math.BigDecimal;

import br.ucsal.bes20231.poo.aula15.imobiliaria.exception.ValorImovelInvalidoException;

/*
 * casas a área de terreno (número fracionário) e a área construída (número fracionário)
 */

public class Casa extends Imovel {

	// 1. Constantes (atributos final)
	// 2. Atributos
	// 3. Construtores
	// 4. Métodos específicos públicos (négocio)
	// 4.1. Métodos privados (algumas equipes organizam os métodos privados antes dos getters/setters)
	// 5. Métodos getters/setters (públicos/protegidos)
	// 6. HashCode e Equals
	// 7. ToString
	// 8. Métodos privados (algumas equipes organizam os métodos privados no final da classe)
	
	private BigDecimal areaTerreno;

	private BigDecimal areaConstruida;

	public Casa(Integer codigo, String endereco, String bairro, BigDecimal valor, BigDecimal areaTerreno,
			BigDecimal areaConstruida) throws ValorImovelInvalidoException {
		super(codigo, endereco, bairro, valor);
		this.areaTerreno = areaTerreno;
		this.areaConstruida = areaConstruida;
	}

	@Override
	public BigDecimal calcularValorImposto() {
		// return areaConstruida * 220 + areaTerreno * 5;
		return areaConstruida.multiply(new BigDecimal(220)).add(areaTerreno.multiply(new BigDecimal(5)));
	}
	
	public BigDecimal getAreaTerreno() {
		return areaTerreno;
	}

	public void setAreaTerreno(BigDecimal areaTerreno) {
		this.areaTerreno = areaTerreno;
	}

	public BigDecimal getAreaConstruida() {
		return areaConstruida;
	}

	public void setAreaConstruida(BigDecimal areaConstruida) {
		this.areaConstruida = areaConstruida;
	}

	@Override
	public String toString() {
		return "Casa [areaTerreno=" + areaTerreno + ", areaConstruida=" + areaConstruida + ", toString()="
				+ super.toString() + "]";
	}

	
	
}
