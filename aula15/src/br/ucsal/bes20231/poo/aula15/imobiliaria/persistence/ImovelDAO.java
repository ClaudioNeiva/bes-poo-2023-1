package br.ucsal.bes20231.poo.aula15.imobiliaria.persistence;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import br.ucsal.bes20231.poo.aula15.imobiliaria.domain.Imovel;

public class ImovelDAO {

	private static List<Imovel> imoveis = new ArrayList<>();

	public static void insert(Imovel imovel) {
		imoveis.add(imovel);
	}

	public static List<Imovel> findAllOrderByBairroValor() {
		List<Imovel> imoveisOrdenados = new ArrayList<>(imoveis);
		imoveisOrdenados.sort(Comparator.comparing(Imovel::getBairro).thenComparing(Imovel::getValor));
		return imoveisOrdenados;
	}

	public static List<Imovel> findAllOrderByCodigo() {
		List<Imovel> imoveisOrdenados = new ArrayList<>(imoveis);
		imoveisOrdenados.sort(Comparator.naturalOrder());
		return imoveisOrdenados;
	}

}
