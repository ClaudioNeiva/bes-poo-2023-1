package br.ucsal.bes20231.poo.aula13.avaliacaounidade1;

public class ExemploUtil {
	
	private ExemploUtil() {
		throw new IllegalStateException("Utility class");
	}

	public static void fazAlgumaCoisa() {
		System.out.println("fiz alguma coisa");
	}
	
	public static void fazOutraCoisa() {
		System.out.println("fiz outra coisa");
	}
	
}
