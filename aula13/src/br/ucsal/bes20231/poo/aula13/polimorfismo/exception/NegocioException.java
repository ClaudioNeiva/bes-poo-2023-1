package br.ucsal.bes20231.poo.aula13.polimorfismo.exception;

public class NegocioException extends Exception {

	private static final long serialVersionUID = 1L;

	public NegocioException(String message) {
		super(message);
	}

}
