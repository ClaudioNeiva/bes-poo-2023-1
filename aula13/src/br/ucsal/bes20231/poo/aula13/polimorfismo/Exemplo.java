package br.ucsal.bes20231.poo.aula13.polimorfismo;

import br.ucsal.bes20231.poo.aula13.polimorfismo.domain.ContaCorrente;
import br.ucsal.bes20231.poo.aula13.polimorfismo.domain.ContaCorrenteEspecial;
import br.ucsal.bes20231.poo.aula13.polimorfismo.domain.ContaRico;
import br.ucsal.bes20231.poo.aula13.polimorfismo.exception.NegocioException;

public class Exemplo {

	public static void main(String[] args) throws NegocioException {

		executarLogicaExemplo();

	}

	private static void executarLogicaExemplo() throws NegocioException {

		ContaCorrenteEspecial contaCorrenteEspecial1 = new ContaCorrenteEspecial(123, "claudio", 500d);
		contaCorrenteEspecial1.depositar(1000.);
//		contaCorrenteEspecial1.sacar(10.);

		ContaCorrente contaCorrente1 = new ContaCorrente(234, "pedro");
		contaCorrente1.depositar(600.);
//		contaCorrente1.sacar(10.);

		ContaRico contaRico1 = new ContaRico(456, "eike");
		contaRico1.depositar(0.);
//		contaRico1.sacar(10.);

		realizarSaque(contaCorrenteEspecial1);
		realizarSaque(contaCorrente1);
		realizarSaque(contaRico1);
		realizarSaque(null);

	}

	private static void realizarSaque(ContaCorrente contaCorrente) {

		// NÃO DEVE SER FEITO ASSIM!
		// Se o método é estático (de classe, static) ele deveria ser chamado a partir
		// da classe e NÃO de uma instância.
		// Como foi escrito errado, o compilador, antes de executar o programa,
		// substitui a linha abaixo por ContaCorrente.fazerAlgo(), e toma a decisão a
		// partir da observação da classe que define o tipo da variável contaCorrente.
		// NÃO NÃO NÃO chame métodos estáticos a partir de variáveis.
		// SEMPRE SEMPRE SEMPRE chame a partir da classe que o define.
		contaCorrente.fazerAlgo();

		try {

			contaCorrente.sacar(1200.);

			System.out.println("saque realizado com sucesso! conta: " + contaCorrente.getNomeCorrentista());

			System.out.println("saldo da conta corrente de " + contaCorrente.getNomeCorrentista() + " = "
					+ contaCorrente.consultarSaldo());

		} catch (NegocioException e) {

			System.out.println(
					"Erro ao realizar o saque: " + e.getMessage() + "conta: " + contaCorrente.getNomeCorrentista());

		}
	}

}
