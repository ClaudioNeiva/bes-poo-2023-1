package br.ucsal.bes20231.poo.aula13.polimorfismo.domain;

import br.ucsal.bes20231.poo.aula13.polimorfismo.exception.NegocioException;

public class ContaCorrente {

	private Integer numero;

	private String nomeCorrentista;

	protected Double saldo;

	private Boolean isBloqueada;

	public ContaCorrente(Integer numero, String nomeCorrentista) {
		super();
		this.numero = numero;
		this.nomeCorrentista = nomeCorrentista;
		this.saldo = 0.;
		this.isBloqueada = false;
	}

	public static void fazerAlgo() {
		System.out.println("fiz algo na ContaCorrente");
	}
	
	public void depositar(Double valor) {
		saldo += valor;
	}

	public void sacar(Double valor) throws NegocioException  {
		verificarSituacaoConta();
		if (valor > saldo) {
			throw new NegocioException("Saldo insuficiente.");
		}
		saldo -= valor;
	}

	protected void verificarSituacaoConta() throws NegocioException {
		if (isBloqueada) {
			throw new NegocioException("Conta bloqueada.");
		}
	}

	public Double consultarSaldo() {
		return saldo;
	}

	public Integer getNumero() {
		return numero;
	}

	public String getNomeCorrentista() {
		return nomeCorrentista;
	}

}
