package br.ucsal.bes20231.poo.aula07.pacote2;

import br.ucsal.bes20231.poo.aula07.Veiculo;

public class Exemplo {

	public static void main(String[] args) {

		Veiculo veiculo1 = new Veiculo("abc1234", 2018);

		veiculo1.setPlaca("abc1234");

		System.out.println("veiculo1.placa=" + veiculo1.getPlaca());

		System.out.println(veiculo1);

		String descricaoVeiculo = "veiculo1=" + veiculo1;
		System.out.println(descricaoVeiculo);

	}

}
