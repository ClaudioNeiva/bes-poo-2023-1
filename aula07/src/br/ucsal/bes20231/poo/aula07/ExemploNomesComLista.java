package br.ucsal.bes20231.poo.aula07;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ExemploNomesComLista {

	private static final int QTD_NOMES = 5;

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		String tipoLista;
		List<String> nomes = new ArrayList<>();

		System.out.println("Informe " + QTD_NOMES + ":");
		for (int i = 0; i < QTD_NOMES; i++) {
			nomes.add(scanner.nextLine());
		}

		System.out.println("Informe um nome a ser removido:");
		String nomeRemover = scanner.nextLine();

		nomes.remove(nomeRemover);

		System.out.println("Lista sem o nome removido:");
		System.out.println(nomes);

		System.out.println("Informe um nome a ser pesquisado:");
		String nomePesquisar = scanner.nextLine();
		if (nomes.contains(nomePesquisar)) {
			int posicaoElemento = nomes.indexOf(nomePesquisar);
			System.out.println("O nome " + nomePesquisar + " existe na lista e está na posição " + posicaoElemento);
		} else {
			System.out.println("O nome " + nomePesquisar + " NÃO existe na lista");
		}

		System.out.println("Tamanho da lista = " + nomes.size());

		nomes.clear();

		System.out.println("Lista depois da limpeza: " + nomes);

		for (int i = 0; i < nomes.size(); i++) {
			System.out.println(nomes.get(i));
		}
		
		for (String nome : nomes) {
			System.out.println(nome);
		}

		nomes.stream().forEach(System.out::println);
		
	}

}
